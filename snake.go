package main

import (
	"fmt"
	tl "github.com/JoelOtter/termloop"
	lang "github.com/hishboy/gocommons/lang"
	"math/rand"
)

var game *tl.Game
var level *tl.BaseLevel
var food *Food
var debugText *tl.Text

var levelWidth = 15
var levelHeight = 15

func main() {
	rand.Seed(42)
	game = tl.NewGame()
	// game.SetDebugOn(true)
	game.SetEndKey(tl.KeyEnd)
	game.Screen().SetFps(60)
	game.Screen().AddEntity(tl.NewFpsText(0, 0, tl.ColorRed, tl.ColorDefault, 0.5))
	// CreateLevel()
	ShowMenu()
	game.Start()
}

func CreateLevel() {
	level = tl.NewBaseLevel(tl.Cell{})
	snake := NewSnake(5, 5)
	snake.SetCell(0, 0, &tl.Cell{Fg: tl.ColorRed, Ch: 'S'})
	level.AddEntity(snake)
	food = &Food{Entity: tl.NewEntity(rand.Intn(10)+3, rand.Intn(10)+3, 1, 1)}
	food.Entity.SetCell(0, 0, &tl.Cell{Fg: tl.ColorGreen, Ch: '*'})
	level.AddEntity(food)
	debugText = tl.NewText(0, 1, "Debug text here", tl.ColorWhite, tl.ColorBlack)
	level.AddEntity(debugText)
	// build wall
	for i := 0; i <= levelWidth; i++ {
		level.AddEntity(&Wall{tl.NewRectangle(i, 2, 1, 1, tl.ColorWhite)})
		level.AddEntity(&Wall{tl.NewRectangle(i, levelHeight+3, 1, 1, tl.ColorWhite)})
	}
	for i := 0; i <= levelHeight; i++ {
		level.AddEntity(&Wall{tl.NewRectangle(0, i+3, 1, 1, tl.ColorWhite)})
		level.AddEntity(&Wall{tl.NewRectangle(levelWidth, i+3, 1, 1, tl.ColorWhite)})
	}
	game.Screen().SetLevel(level)
}

type Snake struct {
	*tl.Entity
	direction       byte
	prevDirection   byte
	moveTickCounter int
	ticksToMove     int
	bodyQueue       *lang.Queue
	bodyLength      int
}

func NewSnake(x, y int) *Snake {
	snake := &Snake{
		Entity:          tl.NewEntity(x, y, 1, 1),
		direction:       'e',
		prevDirection:   'e',
		moveTickCounter: 0,
		ticksToMove:     30,
		bodyQueue:       lang.NewQueue(),
		bodyLength:      4,
	}
	return snake
}

func (snake *Snake) Tick(event tl.Event) {
	if event.Type == tl.EventKey { //keyboard event?
		switch { //which key?
		case event.Key == tl.KeyArrowRight && snake.prevDirection != 'w':
			snake.direction = 'e'
		case event.Key == tl.KeyArrowLeft && snake.prevDirection != 'e':
			snake.direction = 'w'
		case event.Key == tl.KeyArrowUp && snake.prevDirection != 's':
			snake.direction = 'n'
		case event.Key == tl.KeyArrowDown && snake.prevDirection != 'n':
			snake.direction = 's'
		}
		debugText.SetText("Changed direction: " + string(snake.direction))
	}
}

func (snake *Snake) Draw(s *tl.Screen) {
	// move snake after x frames.
	snake.moveTickCounter++
	if snake.moveTickCounter > snake.ticksToMove {
		snake.moveTickCounter = 0
		x, y := snake.Position()
		// add body if it's not at the length
		if snake.bodyQueue.Len() < snake.bodyLength {
			snake.AddBody()
		} else {
			// take the last body and move it to the front
			body, ok := snake.bodyQueue.Poll().(*Body)
			// ok is true
			if ok {
				body.SetPosition(x, y)
			}
			snake.bodyQueue.Push(body)
		}
		// move snake
		switch snake.direction {
		case 'e':
			snake.SetPosition(x+1, y)
		case 'w':
			snake.SetPosition(x-1, y)
		case 'n':
			snake.SetPosition(x, y-1)
		case 's':
			snake.SetPosition(x, y+1)
		}
		snake.prevDirection = snake.direction
	}
	snake.Entity.Draw(s)
}

func (snake *Snake) Collide(collision tl.Physical) {
	// check collision type
	switch v := collision.(type) {
	case *Body:
		// body, game over
		debugText.SetText("Collided with body!")
		ShowMenu()
	case *Wall:
		// wall, game over
		debugText.SetText("Collided with wall!")
		ShowMenu()
	case *Food:
		// food, add body
		debugText.SetText("Eat food!")
		snake.bodyLength++
		// speed up
		snake.ticksToMove--
		// new food
		food.SetPosition(rand.Intn(levelWidth-2)+3, rand.Intn(levelHeight-2)+3)
	default:
		debugText.SetText(fmt.Sprintf("Collided with unknown object: %T", v))
	}
}

func (snake *Snake) AddBody() {
	x, y := snake.Position()
	body := &Body{Entity: tl.NewEntity(x, y, 1, 1)}
	body.SetCell(0, 0, &tl.Cell{Fg: tl.ColorRed, Ch: '@'})
	level.AddEntity(body)
	snake.bodyQueue.Push(body)
}

type Body struct {
	*tl.Entity
}

type Food struct {
	*tl.Entity
}

type Wall struct {
	*tl.Rectangle
}
