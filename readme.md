Go Snake

Snake game made in Go.

Required packages installed with go install:  
github.com/JoelOtter/termloop  
github.com/hishboy/gocommons

How to run from source:  
go run snake.go menu.go
