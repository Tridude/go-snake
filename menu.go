package main

import (
	tl "github.com/JoelOtter/termloop"
	"strconv"
)

var menuLevel = NewMenuLevel()

func ShowMenu() {
	menuLevel.cursor.SetPosition(0, 1)
	game.Screen().SetLevel(menuLevel)
}

type MenuLevel struct {
	*tl.BaseLevel
	widthText *tl.Text
	heightText *tl.Text
	cursor *tl.Entity
}

func NewMenuLevel() *MenuLevel {
	menu := &MenuLevel{
		BaseLevel: tl.NewBaseLevel(tl.Cell{}),
		widthText: tl.NewText(12, 2, strconv.Itoa(levelWidth), tl.ColorWhite, tl.ColorBlack),
		heightText: tl.NewText(12, 3, strconv.Itoa(levelHeight), tl.ColorWhite, tl.ColorBlack),
		cursor: tl.NewEntity(0, 1, 1, 1),
	}
	menu.cursor.SetCell(0, 0, &tl.Cell{Ch: '>'})
	menu.AddEntity(menu.cursor)
	menu.AddEntity(tl.NewText(2, 1, "Play", tl.ColorWhite, tl.ColorBlack))
	menu.AddEntity(tl.NewText(2, 2, "<Width>", tl.ColorWhite, tl.ColorBlack))
	menu.AddEntity(menu.widthText)
	menu.AddEntity(tl.NewText(2, 3, "<Height>", tl.ColorWhite, tl.ColorBlack))
	menu.AddEntity(menu.heightText)
	menu.AddEntity(tl.NewText(2, 4, "[Press End to quit]", tl.ColorWhite, tl.ColorBlack))
	return menu
}

func (menu *MenuLevel) Tick(event tl.Event) {
	if event.Type == tl.EventKey {
		x, y := menu.cursor.Position()
		switch {
			case event.Key == tl.KeyArrowUp && y > 1:
				menu.cursor.SetPosition(x, y-1)
			case event.Key == tl.KeyArrowDown && y < 3:
				menu.cursor.SetPosition(x, y+1)
			case event.Key == tl.KeyArrowLeft && y == 2 && levelWidth > 1:
				levelWidth--
				menu.widthText.SetText(strconv.Itoa(levelWidth))
			case event.Key == tl.KeyArrowRight && y == 2 && levelWidth < 80:
				levelWidth++
				menu.widthText.SetText(strconv.Itoa(levelWidth))
			case event.Key == tl.KeyArrowLeft && y == 3 && levelHeight > 1:
				levelHeight--
				menu.heightText.SetText(strconv.Itoa(levelHeight))
			case event.Key == tl.KeyArrowRight && y == 3 && levelHeight < 22:
				levelHeight++
				menu.heightText.SetText(strconv.Itoa(levelHeight))
			case event.Key == tl.KeyEnter && y == 1:
				CreateLevel()
		}
	}
}